<?php

namespace App\Http\Controllers;

use App\Http\Requests\Brokers\CreateBrokerRequest;
use App\Http\Requests\Brokers\UpdateBrokerRequest;
use App\Models\Broker;
use App\Models\Demat;
use Illuminate\Http\Request;

class BrokersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brokers = Broker::all();
        return view('brokers.index', compact('brokers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('brokers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBrokerRequest $request)
    {
        Broker::create([
            'dp_name' => $request->dp_name,
            'broker_charges' => $request->broker_charges
        ]);
        session()->flash('success', 'Broker Added Successfully!');
        return redirect(route('brokers.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Broker $broker)
    {
        return view('brokers.edit', compact(['broker']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBrokerRequest $request, Broker $broker)
    {
        $broker->dp_name = $request->dp_name;
        $broker->broker_charges = $request->broker_charges;
        $broker->save();

        session()->flash('success','Broker updated successfully!');
        return redirect(route('brokers.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $broker = Broker::findorFail($id);
        $broker->delete();
        session()->flash('success', 'Broker Deleted Successfully!');
        return redirect(route('brokers.index'));
    }
}
