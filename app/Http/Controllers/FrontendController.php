<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Demat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FrontendController extends Controller
{
    public function index()
    {
        $demats = Demat::getdematsbyaccount()->get();
        $total_invested = 0;
        $total_market_value = 0;
        foreach($demats as $demat)
        {
            $total_invested = $demat->total_invested + $total_invested;
            $total_market_value = $demat->total_market_value + $total_market_value;
        }
        $accounts = Account::getbankaccountnumberbyuser();
        return view('dashboard', compact(['demats', 'accounts','total_invested','total_market_value']));
    }
}
