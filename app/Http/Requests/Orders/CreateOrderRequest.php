<?php

namespace App\Http\Requests\Orders;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'demat_id' => 'required|exists:demats,id',
            'stock_id' => 'required|exists:stocks,id',
            'operation' => [
                'required',
                Rule::in(['buy','sell'])
            ],
            'price' => 'required|numeric|min:0',
            'quantity' => 'required|numeric|min:1'
        ];
    }
}
