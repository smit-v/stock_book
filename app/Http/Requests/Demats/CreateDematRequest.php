<?php

namespace App\Http\Requests\Demats;

use Illuminate\Foundation\Http\FormRequest;

class CreateDematRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dp_id'=>'required|min:6|max:10|unique:demats',
            'broker_id'=>'required|exists:brokers,id',
            'dp_account_number'=>'required|min:5',
            'trading_account_number'=>'required|min:6|unique:demats',
        ];
    }
}
