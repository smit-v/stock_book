<?php

namespace App\Http\Requests\Brokers;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBrokerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dp_name'=>'required|unique:brokers,dp_name,'.$this->broker->id,
            'broker_charges' => 'required'
        ];
    }
}
