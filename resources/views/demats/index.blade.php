@extends('layouts.admin-panel.app')
@section('head-name',"Demats")
@section('main-content')
<div class="content">
<div class="container-fluid">


    <div class="d-flex justify-content-end mb-3">
        <a href="{{ route('demats.create') }}" class="btn btn-outline-primary">Add Demat</a>
    </div>
    <div class="card">
        <div class="card-header"><h2>Demats</h2></div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Dp Id</th>
                        <th scope="col">Dp Name</th>
                        <th scope="col">Dp Account Number</th>
                        <th scope="col">Trading Account Number </th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($demats as $demat)
                        <tr>
                            <td>{{ $demat->dp_id }}</td>
                            <td>{{ $demat->broker->dp_name }}</td>
                            <td>{{ $demat->dp_account_number }}</td>
                            <td>{{ $demat->trading_account_number }}</td>
                            <td>
                                <a href="{{ route('demats.edit', $demat->id) }}" class="btn btn-sm btn-primary mr-2">Edit</a>
                                <button type="button" class="btn btn-sm btn-danger" onclick="displayModal({{ $demat->id }})" data-toggle="modal" data-target="#deleteModal">
                                    Delete
                                </button>
                                <a href="{{ route('frontent.portfolio.index', $demat->id) }}" class="btn btn-sm btn-success ml-2">View Portfolio</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</div>
</div>
    <!-- DELETE MODAL -->
    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Modal Title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST" id="deleteDematForm">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        Are you sure, you want to delete this Demat?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-outline-danger">Delete Demat</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="mt-5">
        {{-- {{ $accounts->links('vendor.pagination.bootstrap-4') }} --}}
    </div>
@endsection

@section('page-level-scripts')
    <script>
        function displayModal(dematID){
            var url = "/demats/" + dematID;
            $("#deleteDematForm").attr('action', url);
        }
    </script>
@endsection



