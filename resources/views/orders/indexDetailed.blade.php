@extends('layouts.admin-panel.app')
@section('head-name',"Orders")
@section('main-content')
<div class="content">
    <div class="container-fluid">
        <div class="d-flex justify-content-end mb-3">
            {{-- {{ dd($orders) }} --}}
            <a href="{{ route('frontent.portfolio.index',head($orders)[0][0]->demat_id) }}" class="btn btn-outline-primary">Back</a>
        </div>
        <div class="card">

            <div class="card-header">
                <h2 class="pull-left">Detailed Order List</h2>
                <form action="{{ route('export',head($orders)[0][0]->demat_id) }}" method="GET">
                    @csrf
                    <div class="operation-buttons pull-right">
                        <button type="submit" class="btn btn-outline-success">Download</button>
                    </div>
                </form>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Order Id</th>
                            <th scope="col">Stock</th>
                            <th scope="col">Date</th>
                            <th scope="col">Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Operation</th>
                            <th scope="col">Order Value</th>
                            <th scope="col">Order Id</th>
                            <th scope="col">Date</th>
                            <th scope="col">Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Operation</th>
                            <th scope="col">Order Value</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($orders as $key=>$value)
                            @for ($i=0; $i<count($value) ;$i++)
                            @if ($value[$i][0]->operation == 'buy')

                                <tr>
                                    <td>{{ $value[$i][0]->id }}</td>
                                    <td>{{ $value[$i][0]->stock->stock_name }}</td>

                                    <td>{{ $value[$i][0]->created_at->format('d-m-Y') }}</td>
                                    <td>{{ $value[$i][0]->price }}</td>
                                    <td>{{ $value[$i][0]->quantity }}</td>
                                    <td>{{ $value[$i][0]->operation }}</td>
                                    <td>{{ $value[$i][0]->price * $value[$i][0]->quantity }}</td>

                                @else

                                    @if($value[$i-1][0]->operation == "sell")
                                    </tr>
                                    <tr>
                                        <td scope="col"></td>
                                        <td scope="col"></td>
                                        <td scope="col"></td>
                                        <td scope="col"></td>
                                        <td scope="col"></td>
                                        <td scope="col"></td>
                                        <td scope="col"></td>
                                        <td>{{ $value[$i][0]->id }}</td>
                                        <td>{{ $value[$i][0]->created_at->format('d-m-Y') }}</td>
                                        <td>{{ $value[$i][0]->price }}</td>
                                        <td>{{ $value[$i][0]->quantity }}</td>
                                        <td>{{ $value[$i][0]->operation }}</td>
                                        <td>{{ $value[$i][0]->price * $value[$i][0]->quantity }}</td>
                                    </tr>
                                    @else
                                    <td>{{ $value[$i][0]->id }}</td>
                                    <td>{{ $value[$i][0]->created_at->format('d-m-Y') }}</td>
                                    <td>{{ $value[$i][0]->price }}</td>
                                    <td>{{ $value[$i][0]->quantity }}</td>
                                    <td>{{ $value[$i][0]->operation }}</td>
                                    <td>{{ $value[$i][0]->price * $value[$i][0]->quantity }}</td>
                                    @endif
                            @endif

                            @endfor
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>


    </div>
</div>
<div class="mt-5">
    {{-- {{ $accounts->links('vendor.pagination.bootstrap-4') }} --}}
</div>
@endsection

@section('page-level-scripts')
{{--
<script type="text/javascript">
    function download_csv(csv, filename)
    {
    var csvFile;
    var downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], {type: "text/csv"});

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
	var csv = [];
	var rows = document.querySelectorAll(".table tr");

    for (var i = 0; i < rows.length; i++) {
		var row = [], cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++)
            row.push(cols[j].innerText);

		csv.push(row.join(","));
	}

    // Download CSV
    download_csv(csv.join("\n"), filename);
}

document.querySelector("button").addEventListener("click", function () {
    var html = document.querySelector(".table").outerHTML;
	export_table_to_csv(html, "table.csv");
});

</script> --}}

@endsection

