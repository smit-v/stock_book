@extends('layouts.admin-panel.app')
@section('head-name',"Dashboard")
@section('page-level-styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('main-content')
<div class="content mb-0">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <h2>Demats</h2>
                </div>
                <div class="card-body">
                    @foreach ($demats as $demat)

                    <div class="card">
                        <div class="card-header">
                            <h5 class="pull-left">DP ID: {{ $demat->dp_id }}</h5>
                            <h5 class="pull-right">Acc no: {{ $demat->account->bank_account_number }}</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <p>Invested: <strong>{{ $demat->total_invested }}</strong></p>
                                    <p>Market Value: <strong>{{ $demat->total_market_value }}</strong></p>
                                </div>
                                <div class="col-md-6 text-center">
                                    <a class="btn btn-primary" href="/portfolio/{{ $demat->id }}">View Portfolio</a>
                                </div>
                            </div>
                        </div>

                    </div>

                    @endforeach
                </div>
                <div class="card-footer">
                    <h4>Total Invested: {{ $total_invested }}</h4>
                    <h4>Total Market Value: {{ $total_market_value }}</h4>
                </div>
            </div>
        </div>
        @if (Auth::check())
        <div class="row">
            <div class="col-sm-6">
                <div class="card text-center">
                    {{-- <form action="" method="GET" id="dematForm"> --}}

                        <div class="card-body">
                            <!-- Example split danger button -->
                            <div class="form-group">
                                <select name="account_id" id="account_id" class="form-control select2Demats">
                                    <option></option>
                                    @foreach ($accounts as $account)
                                        @if($account->id == old('account_id'))
                                            <option value="{{ $account->id }}" selected>{{ $account->bank_account_number }}</option>
                                        @else
                                            <option value="{{ $account->id }}">{{ $account->bank_account_number }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @error('account_id')
                                  <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <form action="" method="GET" id="getDemats">
                                <input type="submit" class="btn btn-outline-primary" id="btn-demats" value="View Demats Account">
                            </form>
                        </div>
                    {{-- </form> --}}
                </div>
            </div>


            <div class="col-sm-6">
                <div class="card text-center">
                    <form action="/aba/" method="GET" id="portfolioForm">

                        <div class="card-body">
                            <!-- Example split danger button -->
                            <div class="form-group">
                                <select name="demat_id" id="demat_id" class="form-control select2">
                                    <option></option>
                                    @foreach ($demats as $demat)
                                        @if($demat->id == old('demat_id'))
                                            <option value="{{ $demat->id }}" selected>{{ $demat->account->bank_account_number." ------ ".$demat->dp_id }}</option>
                                        @else
                                            <option value="{{ $demat->id }}">{{ $demat->account->bank_account_number." ------ ".$demat->dp_id }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @error('demat_id')
                                  <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <a id="btn-portfolio" href="" class="btn btn-outline-primary">View Portfolio</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @endif

    </div>
</div>
@endsection
@section('page-level-scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $('.select2').select2({
        placeholder: 'Select a Demat Account',
        allowClear: true
    });

    $('#btn-portfolio').click(function(e){
        // e.preventDefault();
        selectedId = $("#demat_id").children('option:selected').val();
        if(selectedId == 0)
        {
            return;
        }

        var url = "/portfolio/" + selectedId;
        $("#btn-portfolio").attr('href',url)
    });

    $('.select2Demats').select2({
        placeholder: 'Select a Bank Account',
        allowClear: true
    });

    $('#btn-demats').click(function(e){
        // e.preventDefault();
        selectedId = $("#account_id").children('option:selected').val();
        if(selectedId == 0)
        {
            return;
        }

        var url = "/account/" + selectedId + "/demats";
        $("#getDemats").attr('action',url)
    });
</script>
@endsection
