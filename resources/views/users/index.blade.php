
@extends('layouts.admin-panel.app')
@section('head-name',"User Profile")
@section('main-content')
<div class="content">
    <div class="container-fluid">
        <div class="jumbotron">
            <h1 class="display-7">{{ Auth::user()->name }}</h1>
            <hr class="my-4">
            <p>{{ Auth::user()->email }}</p>
            <p>{{ Auth::user()->phone_number }}</p>
            <a href="{{ route('users.edit', Auth::user()->id) }}" class="btn btn-primary mr-2">Edit Profile</a>
            <a href="{{ route('change.password') }}" class="btn btn-primary mr-2">Change Password</a>
        </div>
    </div>
</div>
@endsection
