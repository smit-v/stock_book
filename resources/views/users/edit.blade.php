@extends('layouts.admin-panel.app')
@section('head-name',"User Profile")
@section('main-content')
<div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header"><h2>Edit User</h2></div>
            <div class="card-body">
                <form action="{{ route('users.update', $user->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text"
                                class="form-control @error('name') is-invalid @enderror"
                                id="name"
                                value="{{ old('name', $user->name) }}"
                                placeholder="Enter Name"
                                name="name">
                            @error('name')
                                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                            @enderror
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email"
                                class="form-control @error('email') is-invalid @enderror"
                                id="email"
                                value="{{ old('email', $user->email) }}"
                                placeholder="Enter Email"
                                name="email">
                            @error('email')
                                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                            @enderror
                    </div>

                    <div class="form-group">
                        <label for="phone_number">Phone Number</label>
                        <input type="number"
                                class="form-control @error('phone_number') is-invalid @enderror"
                                id="phone_number"
                                value="{{ old('phone_number', $user->phone_number) }}"
                                placeholder="Enter Phone Number"
                                name="phone_number">
                            @error('phone_number')
                                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                            @enderror
                    </div>

                    <button type="submit" class="btn btn-outline-success">Submit</button>
                </form>
            </div>
        </div>

    </div>
</div>
@endsection
