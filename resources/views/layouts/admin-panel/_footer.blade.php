<footer class="footer">
    <div class="container-fluid">
        <nav>
            <ul class="footer-menu">
                <li>
                    <a href="{{ route('frontend.index') }}">
                        Home
                    </a>
                </li>
            </ul>
            <p class="copyright text-justify-center">
                ©
                <script>
                    document.write(new Date().getFullYear())
                </script>
                <a href="@">SPY Team</a>, EK dusre ki help karo!!
            </p>
        </nav>
    </div>
</footer>
