{{-- <html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('admin-panel/css/bootstrap.min.css') }}" crossorigin="anonymous">
    @yield('page-level-styles')
    <title>Hello, world!</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="{{ route('dashboard') }}">Stock Book Admin Panel</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>


       <div class="collapse navbar-collapse" id="navbarSupportedContent">
           <ul class="navbar-nav ml-auto">
               <li class="nav-item dropdown">
                   <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       {{ auth()->user()->name }}
                   </a>
                   <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                       <form action="{{ route('logout') }}" method="POST">
                           @csrf
                           <input type="submit" class="dropdown-item" value="Logout">
                       </form>

                   </div>
               </li>
           </ul>
       </div>
   </div>
</nav>
<div class="container py-5">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a href="{{ route('dashboard') }}">Dashboard</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('accounts.index') }}">Accounts</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('demats.index') }}">Demats</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('stocks.index') }}">Stocks</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('orders.index') }}">Orders</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            @include('layouts.partials._message')
            @yield('content')
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="{{ asset('admin-panel/js/bootstrap.min.js') }}" ></script>

@yield('page-level-scripts')
</body>
</html> --}}


<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />
    {{--Images to be linked  --}}
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('frontend/assets/img/apple-icon.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('frontend/assets/img/favicon.ico') }}">
    {{-- Important Meta Tags --}}
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    {{-- Title goes here --}}
    <title>@yield('title')</title>
    <!-- Fonts and icons -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="{{ asset('frontend/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('frontend/assets/css/light-bootstrap-dashboard.css?v=2.0.0') }}" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{ asset('frontend/assets/css/demo.css') }}" rel="stylesheet" />
    <link href="{{ asset('/css/custom-style.css') }}" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" />

    @yield('page-level-styles')
</head>

<body>
    <div class="wrapper">
        @include('layouts.admin-panel._sidebar')
        <div class="main-panel">
            <!-- Navbar -->
            @include('layouts.admin-panel._navigation')

            <div class="col-md-12">
                @include('layouts.partials._message')
            </div>

            @yield('main-content')

            {{-- Footer --}}
            @include('layouts.admin-panel._footer')
        </div>
    </div>

</body>
<!--   Core JS Files   -->
<script src="{{ asset('frontend/assets/js/core/jquery.3.2.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('frontend/assets/js/core/popper.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('frontend/assets/js/core/bootstrap.min.js') }}" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="{{ asset('frontend/assets/js/plugins/bootstrap-switch.js') }}"></script>
<!--  Chartist Plugin  -->
{{-- <script src="{{ asset('frontend/assets/js/plugins/chartist.min.js') }}"></script> --}}
<!--  Notifications Plugin    -->
<script src="{{ asset('frontend/assets/js/plugins/bootstrap-notify.js') }}"></script>
<!-- Control Center for Light Bootstrap Dashboard: scripts for the example pages etc -->
<script src="{{ asset('frontend/assets/js/light-bootstrap-dashboard.js?v=2.0.0') }}" type="text/javascript"></script>
<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
<script src="{{ asset('frontend/assets/js/demo.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        // Javascript method's body can be found in assets/js/demos.js
        // demo.initDashboardPageCharts();
    });
</script>
@yield('page-level-scripts')
</html>

