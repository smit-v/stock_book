@extends('layouts.admin-panel.app')
@section('head-name',"Brokers")
@section('main-content')
<div class="content">
    <div class="container-fluid">

        <div class="card">
            <div class="card-header"><h2>Add New Broker</h2></div>
            <div class="card-body">



                    <div class="form-group">
                        <label for="dp">DP Name</label>
                        <input type="text"
                                class="form-control @error('dp') is-invalid @enderror"
                                style="text-transform:uppercase"
                                id="dp"
                                value="{{ old('dp') }}"
                                placeholder="Enter DP Name"
                                onkeyup="setDpName()"
                                >

                    </div>

                    <div class="form-group">
                        <label for="dp">Type</label>
                        <select id="type" class="form-control" onchange="setDpName()">
                            <option value="1">Standard</option>
                            <option value="2">Deluxe</option>
                            <option value="3">Executive</option>
                        </select>
                        @error('type')
                          <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                <form action="{{ route('brokers.store') }}" method="POST">
                    @csrf

                    <div class="form-group">
                        <label for="dp_name">Dp Name constructed</label>
                        <input type="text"
                                class="form-control disabled"
                                id="dp_name"
                                value=""
                                name="dp_name"
                                >
                    </div>
                    @error('dp_name')
                                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror

                    <div class="form-group">
                        <label for="broker_charges">Broker Charges in %</label>
                        <input type="decimal"
                                class="form-control @error('broker_charges') is-invalid @enderror"
                                id="broker_charges"
                                value="{{ old('broker_charges') }}"
                                placeholder="Enter Broker Charges"
                                name="broker_charges">
                            @error('broker_charges')
                                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                            @enderror
                    </div>
                    <button type="submit" class="btn btn-outline-success">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-level-scripts')

<script>
    function setDpName()
    {
        dp = $("#dp").val();
        if(dp == "")
        {
            alert("Please give some dp name first");
            return;
        }
        type = $("#type").val().toUpperCase();
        $("#dp_name").attr("value",dp+" "+type);
    }
</script>

@endsection


