@extends('layouts.admin-panel.app')
@section('head-name',"Brokers")
@section('main-content')
<div class="content">
    <div class="container-fluid">

        <div class="d-flex justify-content-end mb-3">
            <a href="{{ route('brokers.create') }}" class="btn btn-outline-primary">Add Broker</a>
        </div>
        <div class="card">
            <div class="card-header"><h2>Brokers</h2></div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col-4">DP Name</th>
                            <th scope="col-4">Broker Charges</th>
                            <th scope="col-4">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($brokers as $broker)
                            <tr>
                                <td>{{ $broker->dp_name }}</td>
                                <td>{{ $broker->broker_charges }} %</td>
                                <td>
                                    <a href="{{ route('brokers.edit', $broker->id) }}" class="btn btn-sm btn-primary mr-2">Edit</a>
                                    <button type="button" class="btn btn-sm btn-danger" onclick="displayModal({{ $broker->id }})" data-toggle="modal" data-target="#deleteModal">
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>


    </div>
</div>

<!-- DELETE MODAL -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Modal Title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="POST" id="deleteBrokerForm">
                @csrf
                @method('DELETE')
                <div class="modal-body">
                    Are you sure, you want to delete this broker?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline-danger">Delete Broker</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="mt-5">
    {{-- {{ $stocks->links('vendor.pagination.bootstrap-4') }} --}}
</div>
@endsection

@section('page-level-scripts')
    <script>
        function displayModal(brokerId){
            var url = "/brokers/" + brokerId;
            $("#deleteBrokerForm").attr('action', url);
        }
    </script>
@endsection


