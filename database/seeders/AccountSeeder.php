<?php

namespace Database\Seeders;

use App\Models\Account;
use Illuminate\Database\Seeder;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Account::create([
            'aadhar_number' => '803371749909',
            'pan_number' => 'FDHPM4458P',
            'bank_name' => "HDFC Bank",
            'bank_account_number' => 50100388001130,
            'user_id' => 1
        ]);

        Account::create([
            'aadhar_number' => '803371741101',
            'pan_number' => 'FDHPM2258P',
            'bank_name' => "Yes Bank",
            'bank_account_number' => 50100388001110,
            'user_id' => 1
        ]);

        Account::create([
            'aadhar_number' => '803371742202',
            'pan_number' => 'FDHPM2358P',
            'bank_name' => "Axis Bank",
            'bank_account_number' => 50100388001120,
            'user_id' => 2
        ]);

        Account::create([
            'aadhar_number' => '803371743303',
            'pan_number' => 'FDHPM5558P',
            'bank_name' => "ICICI Bank",
            'bank_account_number' => 50100388001140,
            'user_id' => 2
        ]);

        Account::create([
            'aadhar_number' => '803371744404',
            'pan_number' => 'FDHPM6558P',
            'bank_name' => "State Bank Of India",
            'bank_account_number' => 50100388001150,
            'user_id' => 2
        ]);

        Account::create([
            'aadhar_number' => '803371745505',
            'pan_number' => 'FDHPM7758P',
            'bank_name' => "HDFC Bank",
            'bank_account_number' => 50100388001160,
            'user_id' => 3
        ]);

        Account::create([
            'aadhar_number' => '803371746606',
            'pan_number' => 'FDHPM8858P',
            'bank_name' => "Yes Bank",
            'bank_account_number' => 50100388001170,
            'user_id' => 3
        ]);

        Account::create([
            'aadhar_number' => '803371747707',
            'pan_number' => 'FDHPM9958P',
            'bank_name' => "State Bank Of India",
            'bank_account_number' => 50100388001180,
            'user_id' => 3
        ]);

        Account::create([
            'aadhar_number' => '803371748808',
            'pan_number' => 'FDHPM1058P',
            'bank_name' => "ICICI Bank",
            'bank_account_number' => 50100388001190,
            'user_id' => 4
        ]);

        Account::create([
            'aadhar_number' => '803371741111',
            'pan_number' => 'FDHPM1258P',
            'bank_name' => "Axis Bank",
            'bank_account_number' => 50100388001310,
            'user_id' => 4
        ]);

        Account::create([
            'aadhar_number' => '803371742222',
            'pan_number' => 'FDHPM1358P',
            'bank_name' => "Yes Bank",
            'bank_account_number' => 50100388001320,
            'user_id' => 4
        ]);

        Account::create([
            'aadhar_number' => '803371743333',
            'pan_number' => 'FDHPM1458P',
            'bank_name' => "HDFC Bank",
            'bank_account_number' => 50100388001330,
            'user_id' => 4
        ]);

    }
}
